const gulp = require('gulp');

const path = require('./tasks/config');

const requireDir = require('require-dir');
requireDir('./tasks', { recurse: true });

gulp.task('build',
	gulp.series('clean',
	gulp.parallel(
	              'pug-build',
	              'scss-build',
	              'ts-build')));

gulp.task('watch', () => {
	gulp.watch(path.watch.pug, gulp.series('pug-build'));
	gulp.watch(path.watch.scss, gulp.series('scss-build'));
	gulp.watch(path.watch.img, gulp.series('img-background'));
	gulp.watch(path.watch.sprite, gulp.series('sprite'));
});

gulp.task('default',
	gulp.series('build',
	gulp.parallel(
	              'img-background',
	              'sprite',
	              'browser-sync',
	              'watch')));

gulp.task('gh-pages',
	gulp.series('build',
	gulp.parallel(
	              'img-background',
	              'sprite')));
