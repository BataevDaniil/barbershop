export default class Slider {
	private count:number = 0;
	private timer:any;
	private stoping:boolean = true;
	private firstStartMedia:boolean = false;
	private toggleHandler:any = [];
	private previusButton:any = null;
	private nextButton:any = null;

	constructor(private slider:any,
	            private toggle:any,
	            private maxCount:number = toggle.length,
	            public timeToggle:number = 5000,
	            private media:string = '') {
		if (toggle)
			for (let i = 0; i < this.maxCount; i++)
				this.toggleHandler[i] = () => {
					this.stop();
					this.start(i);
					console.log('toggle');
				};
		if (media) {
			this.firstStartMedia = window.matchMedia(media).matches;
			window.addEventListener('resize', () => {
				if (window.matchMedia(media).matches && !this.firstStartMedia)
					this.start();
				if (!(this.firstStartMedia= window.matchMedia(media).matches))
					this.stop();
			});
		}
	}

	private setPosSlider = (n:number) => {
		if (this.toggle) {
			this.toggle[this.count % this.maxCount].classList.remove('is-active');
			this.toggle[(this.count = n) % this.maxCount].classList.add('is-active');
		}
		else
			this.count = n;
		this.slider.style['margin-left'] = `-${this.count % this.maxCount}00%`;
		console.log('shoftSlider');
	};

	private startTimer(n:number = 0) {
		this.setPosSlider(n);
		this.timer = setInterval( () => this.setPosSlider(this.count + 1), this.timeToggle);
	}

	private stopTimer() {
		clearInterval(this.timer);
	}

	public start(n:number = 0) {
		if (this.stoping) {
			if (this.toggle)
				for (let i = 0; i < this.maxCount; i++)
					this.toggle[i].addEventListener('click', this.toggleHandler[i]);

			if (this.previusButton)
				this.previusButton.addEventListener('click', this.previus);

			if (this.nextButton)
				this.nextButton.addEventListener('click', this.next);

			this.startTimer(n);
			this.stoping = false;
			console.log('start');
		}
	}

	public stop() {
		if (!this.stoping) {
			if (this.toggle)
				for (let i = 0; i < this.maxCount; i++)
					this.toggle[i].removeEventListener('click', this.toggleHandler[i]);

			if (this.previusButton)
				this.previusButton.removeEventListener('click', this.previus);

			if (this.nextButton)
				this.nextButton.removeEventListener('click', this.next);

			this.stopTimer();
			this.stoping = true;
			console.log('stop');
		}
	}

	private previus = () => {
			this.stopTimer();
			if (this.count <= 0)
				this.startTimer( this.maxCount - 1);
			else
				this.startTimer(this.count - 1);
			console.log('previus');
	};

	private next = () => {
		this.stopTimer();
		this.startTimer(this.count + 1);
		console.log('next');
	};

	public setPreviusButton(button:any) {
		this.previusButton = button;
	}

	public setNextButton(button:any) {
		this.nextButton = button;
	}
}