import Slider from './slider'

let sliderPlus:Slider = new Slider(
	document.querySelector('.plus-list'),
	document.querySelectorAll('.plus .slider-toggle li'),
	3,
	5000,
	'(max-width: 768px)'
);
sliderPlus.start();

let sliderReviews:Slider = new Slider(
	document.querySelector('.reviews-slider-wrapper'),
	document.querySelectorAll('.reviews-toggle-slider button'),
	3,
	5000,
);
sliderReviews.setPreviusButton(document.querySelector('.reviews-btn-previus'));
sliderReviews.setNextButton(document.querySelector('.reviews-btn-next'));
sliderReviews.start();


//button navitagion
(<any>document.querySelector('.btn-nav')).onclick = () => {
	let nav:any = document.querySelector('.main-nav');
	nav.classList.toggle('is-closed');
	nav.classList.toggle('is-opened');
};

//button view all
(<any>document.querySelector('.btn-view-all')).onclick = function () {
	let news:any = document.querySelectorAll('.news-list li.is-hidden');
	console.log(news);
	for (let i = 0; i < news.length; i++) {
		let style = getComputedStyle(news[i]);
		if(style.display === 'none') {
			news[i].style.display = 'flex';
			this.innerText = 'Скрыть всё';
		}
		else {
			news[i].style.display = '';
			this.innerText = 'Показать всё';
		}
	}
};
